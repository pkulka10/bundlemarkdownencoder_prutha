# Bundle Markdown Encoder

A maven plugin to encode the contents of a README.md file into the "Bundle-Description" attribute of an OSGi bundles manifest. This makes it possible for IGB's App Manager compomnent to obtain a rich html description of a bundle directly from an Object Bundle Repository (OBR). The IGB project is using OBR as the format for plugin repositories. This maven plugin lets allow IGB App developers leverage Markdown as convenient way to describe their App to users.

Example Usage:

```pom
<plugin>
  <groupId>com.lorainelab</groupId>
  <artifactId>bundle-markdown-encoder</artifactId>
  <version>0.0.1</version> 
   <executions>
      <execution>
        <goals>
          <goal>encodeMarkdown</goal>
        </goals>
      </execution> 
    </executions>
</plugin>   
```
